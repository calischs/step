## Printed stepper motor

The motor below was printed using <a href='https://www.proto-pasta.com/pages/magnetic-iron-pla'>Proto Pasta's Iron-doped PLA</a>, which has a relative permeability of around 10.

<img src='fusion-design.png' width=300px>
<img src='printed-stepper.jpg' width=340px>

This directory contains STLs and a Fusion design file.

Be warned -- there are a few crucial design errors!  First, the tooth size is too small given the reduced relative permeability of this filament, as compared to magnetic steel ($`\mu_r \approx 1000`$).  Second, while the two magnetic poles have a full tooth offset, the two electrical phases don't have a half tooth offset.  Duh!  So, even if the tooth size we correct, the stepper wouldn't step in a predicatble direction.  Someday I'll fix these and print another...