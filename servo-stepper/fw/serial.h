#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#define output(directions,pin) (directions |= pin) // set port direction for output
#define set(port,pin) (port |= pin) // set port pin
#define clear(port,pin) (port &= (~pin)) // clear port pin
#define pin_test(pins,pin) (pins & pin) // test for port pin
#define bit_test(byte,bit) (byte & (1 << bit)) // test for bit set

/* USART buffer defines. */
/* \brief  Receive buffer size: 2,4,8,16,32,64,128 or 256 bytes. */
#define USART_RX_BUFFER_SIZE 128
/* \brief Transmit buffer size: 2,4,8,16,32,64,128 or 256 bytes */
#define USART_TX_BUFFER_SIZE 128
/* \brief Receive buffer mask. */
#define USART_RX_BUFFER_MASK ( USART_RX_BUFFER_SIZE - 1 )
/* \brief Transmit buffer mask. */
#define USART_TX_BUFFER_MASK ( USART_TX_BUFFER_SIZE - 1 )

#if ( USART_RX_BUFFER_SIZE & USART_RX_BUFFER_MASK )
#error RX buffer size is not a power of 2
#endif
#if ( USART_TX_BUFFER_SIZE & USART_TX_BUFFER_MASK )
#error TX buffer size is not a power of 2
#endif

/* \brief USART transmit and receive ring buffer. */
typedef struct USART_Buffer
{
   /* \brief Receive buffer. */
   volatile uint8_t RX[USART_RX_BUFFER_SIZE];
   /* \brief Transmit buffer. */
   volatile uint8_t TX[USART_TX_BUFFER_SIZE];
   /* \brief Receive buffer head. */
   volatile uint8_t RX_Head;
   /* \brief Receive buffer tail. */
   volatile uint8_t RX_Tail;
   /* \brief Transmit buffer head. */
   volatile uint8_t TX_Head;
   /* \brief Transmit buffer tail. */
   volatile uint8_t TX_Tail;
} USART_Buffer_t;


/*! \brief Struct used when interrupt driven driver is used.
*
*  Struct containing pointer to a usart, a buffer and a location to store Data
*  register interrupt level temporary.
*/
typedef struct Usart_and_buffer
{
   /* \brief Pointer to USART module to use. */
   USART_t * usart;
   /* \brief Data register empty interrupt level. */
   USART_DREINTLVL_t dreIntLevel;
   /* \brief Data buffer. */
   USART_Buffer_t buffer;
} USART_data_t;

#define USART_RxdInterruptLevel_Set(_usart, _rxdIntLevel)                      \
   ((_usart)->CTRLA = ((_usart)->CTRLA & ~USART_RXCINTLVL_gm) | _rxdIntLevel)
#define USART_Format_Set(_usart, _charSize, _parityMode, _twoStopBits)         \
   (_usart)->CTRLC = (uint8_t) _charSize | _parityMode |                      \
                     (_twoStopBits ? USART_SBMODE_bm : 0)
#define USART_Baudrate_Set(_usart, _bselValue, _bScaleFactor)                  \
   (_usart)->BAUDCTRLA =(uint8_t)_bselValue;                                           \
   (_usart)->BAUDCTRLB =(_bScaleFactor << USART_BSCALE0_bp)|(_bselValue >> 8)
#define USART_Rx_Enable(_usart) ((_usart)->CTRLB |= USART_RXEN_bm)
#define USART_Rx_Disable(_usart) ((_usart)->CTRLB &= ~USART_RXEN_bm)
#define USART_Tx_Enable(_usart)  ((_usart)->CTRLB |= USART_TXEN_bm)
#define USART_Tx_Disable(_usart) ((_usart)->CTRLB &= ~USART_TXEN_bm)



_Bool USART_TXBuffer_FreeSpace(USART_data_t * usart_data)
{
   /* Make copies to make sure that volatile access is specified. */
   uint8_t tempHead = (usart_data->buffer.TX_Head + 1) & USART_TX_BUFFER_MASK;
   uint8_t tempTail = usart_data->buffer.TX_Tail;

   /* There are data left in the buffer unless Head and Tail are equal. */
   return (tempHead != tempTail);
}
_Bool USART_TXBuffer_PutByte(USART_data_t * usart_data, uint8_t data)
{
   uint8_t tempCTRLA;
   uint8_t tempTX_Head;
   _Bool TXBuffer_FreeSpace;
   USART_Buffer_t * TXbufPtr;

   TXbufPtr = &usart_data->buffer;
   TXBuffer_FreeSpace = USART_TXBuffer_FreeSpace(usart_data);


   if(TXBuffer_FreeSpace)
   {
      tempTX_Head = TXbufPtr->TX_Head;
      TXbufPtr->TX[tempTX_Head]= data;
      /* Advance buffer head. */
      TXbufPtr->TX_Head = (tempTX_Head + 1) & USART_TX_BUFFER_MASK;

      /* Enable DRE interrupt. */
      tempCTRLA = usart_data->usart->CTRLA;
      tempCTRLA = (tempCTRLA & ~USART_DREINTLVL_gm) | usart_data->dreIntLevel;
      usart_data->usart->CTRLA = tempCTRLA;
   }
   return TXBuffer_FreeSpace;
}
_Bool USART_RXBufferData_Available(USART_data_t * usart_data)
{
   /* Make copies to make sure that volatile access is specified. */
   uint8_t tempHead = usart_data->buffer.RX_Head;
   uint8_t tempTail = usart_data->buffer.RX_Tail;

   /* There are data left in the buffer unless Head and Tail are equal. */
   return (tempHead != tempTail);
}
uint8_t USART_RXBuffer_GetByte(USART_data_t * usart_data)
{
   USART_Buffer_t * bufPtr;
   uint8_t ans;
   bufPtr = &usart_data->buffer;
   ans = (bufPtr->RX[bufPtr->RX_Tail]);

   /* Advance buffer tail. */
   bufPtr->RX_Tail = (bufPtr->RX_Tail + 1) & USART_RX_BUFFER_MASK;
   return ans;
}
_Bool USART_RXComplete(USART_data_t * usart_data)
{
   USART_Buffer_t * bufPtr;
   _Bool ans;

   bufPtr = &usart_data->buffer;
   /* Advance buffer head. */
   uint8_t tempRX_Head = (bufPtr->RX_Head + 1) & USART_RX_BUFFER_MASK;

   /* Check for overflow. */
   uint8_t tempRX_Tail = bufPtr->RX_Tail;
   uint8_t data = usart_data->usart->DATA;

   if (tempRX_Head == tempRX_Tail) {
      ans = 0;
   }else{
      ans = 1;
      usart_data->buffer.RX[usart_data->buffer.RX_Head] = data;
      usart_data->buffer.RX_Head = tempRX_Head;
   }
   return ans;
}
void USART_DataRegEmpty(USART_data_t * usart_data)
{
   USART_Buffer_t * bufPtr;
   bufPtr = &usart_data->buffer;

   /* Check if all data is transmitted. */
   uint8_t tempTX_Tail = usart_data->buffer.TX_Tail;
   if (bufPtr->TX_Head == tempTX_Tail){
       /* Disable DRE interrupts. */
      uint8_t tempCTRLA = usart_data->usart->CTRLA;
      tempCTRLA = (tempCTRLA & ~USART_DREINTLVL_gm) | USART_DREINTLVL_OFF_gc;
      usart_data->usart->CTRLA = tempCTRLA;

   }else{
      /* Start transmitting. */
      uint8_t data = bufPtr->TX[usart_data->buffer.TX_Tail];
      usart_data->usart->DATA = data;

      /* Advance buffer tail. */
      bufPtr->TX_Tail = (bufPtr->TX_Tail + 1) & USART_TX_BUFFER_MASK;
   }
}
void USART_InterruptDriver_Initialize(USART_data_t * usart_data,
                                      USART_t * usart,
                                      USART_DREINTLVL_t dreIntLevel)
{
   usart_data->usart = usart;
   usart_data->dreIntLevel = dreIntLevel;

   usart_data->buffer.RX_Tail = 0;
   usart_data->buffer.RX_Head = 0;
   usart_data->buffer.TX_Tail = 0;
   usart_data->buffer.TX_Head = 0;
}
void USART_InterruptDriver_DreInterruptLevel_Set(USART_data_t * usart_data,
                                                 USART_DREINTLVL_t dreIntLevel)
{
   usart_data->dreIntLevel = dreIntLevel;
}

void usart_send_byte(USART_data_t * usart_data, char msg){
   while(!USART_TXBuffer_FreeSpace(usart_data)){}
   USART_TXBuffer_PutByte(usart_data,msg);
}
void usart_send_uint32(USART_data_t * usart_data, uint32_t n){
   unsigned char * nn = (unsigned char *)&n;
   usart_send_byte(usart_data, nn[0] );
   usart_send_byte(usart_data, nn[1] );
   usart_send_byte(usart_data, nn[2] );
   usart_send_byte(usart_data, nn[3] );
}
void usart_send_int32(USART_data_t * usart_data, int32_t n){
   unsigned char * nn = ( unsigned char *)&n;
   usart_send_byte(usart_data, nn[0] );
   usart_send_byte(usart_data, nn[1] );
   usart_send_byte(usart_data, nn[2] );
   usart_send_byte(usart_data, nn[3] );   
}
void usart_send_uint16(USART_data_t * usart_data, uint16_t n){
   unsigned char * nn = (unsigned char *)&n;
   usart_send_byte(usart_data, nn[0] );
   usart_send_byte(usart_data, nn[1] );
}
void usart_send_int16(USART_data_t * usart_data, int16_t n){
   unsigned char * nn = ( unsigned char *)&n;
   usart_send_byte(usart_data, nn[0] );
   usart_send_byte(usart_data, nn[1] );  
}
uint32_t parse_uint32(char* b){return *(uint32_t *) b;}
int32_t parse_int32(char* b){return *(int32_t *) b;}
uint16_t parse_uint16(char* b){return *(uint16_t *) b;}


/*
void usart_write_buffer(char* buffer){
   //write \n terminated buffer over usart
   int j=0;
   do{
      usart_send_byte( buffer[j] );
      j++;
   } while( j < PACKET_SIZE);
   //} while( buffer[j-1] != 10);
}
*/


